import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Server {
	public static HashMap<String, Object> Login() {
		HashMap<String, Object> map = new HashMap<>();
		HashMap<String, Object> content = new HashMap<>();
		// add content fields
		content.put("login", true);
		content.put("username", " user1");
		content.put("password", "pass");
		// add map fields
		map.put("packet_type", "Login");
		map.put("content", content);
		return map;
	}

	public static HashMap<String, Object> sendLoginAccepted(boolean status) {
		HashMap<String, Object> map = new HashMap<>();
		HashMap<String, Object> content = new HashMap<>();
		HashMap<String, Object> settings = new HashMap<>();
		// add content fields
		content.put("status", status);
		content.put("error", 0);
		// add settings fields
		settings.put("Username", "user1");
		settings.put("SessionId", "Id Sesiune");
		settings.put("FirstName", "Maria");
		settings.put("LastName", "Ioana");
		settings.put("Id", 123);
		settings.put("DepartmentId", 1000);
		// add map fields
		map.put("packet_type", "login_ack");
		map.put("content", content);
		map.put("settings", settings);
		return map;
	}

	public static HashMap<String, Object> sendLogoutConfirmed() {
		HashMap<String, Object> map = new HashMap<>();
		HashMap<String, Object> content = new HashMap<>();
		// add content fields
		content.put("status", true);
		content.put("error", 0);
		// add map fields
		map.put("packet_type", "logout_ack");
		map.put("content", content);
		return map;
	}

	public static HashMap<String, Object> biometricsMap(int heart, int blood, int body, int oxygen) {
		HashMap<String, Object> biometrics = new HashMap<>();
		HashMap<String, Object> blood_presure = new HashMap<>();
		HashMap<String, Object> heart_rate = new HashMap<>();
		HashMap<String, Object> O2 = new HashMap<>();
		HashMap<String, Object> body_temperature = new HashMap<>();
		blood_presure.put("value", blood);
		blood_presure.put("alarm", false);
		heart_rate.put("value", heart);
		heart_rate.put("alarm", false);
		O2.put("value", oxygen);
		O2.put("alarm", false);
		body_temperature.put("value", body);
		body_temperature.put("alarm", false);
		biometrics.put("blood_presure", blood_presure);
		biometrics.put("heart_rate", heart_rate);
		biometrics.put("o2", O2);
		biometrics.put("body_temperature", body_temperature);
		return biometrics;
	}

	public static HashMap<String, Object> patientMap(String cerinta, String name, int floor, int room, int bed,
			int heart_rate, int blood_presure, int body_temperature, int oxygen_saturation) {
		HashMap<String, Object> patient = new HashMap<>();
		HashMap<String, Object> biometrics = biometricsMap(heart_rate, blood_presure, body_temperature,
				oxygen_saturation);
		HashMap<String, Object> request = new HashMap<>();
		request.put("value", cerinta);
		request.put("alarm", true);
		patient.put("id", "df54");
		patient.put("name", name);
		patient.put("floor", floor);
		patient.put("room", room);
		patient.put("bed", bed);
		patient.put("request", request);
		patient.put("biometrics", biometrics);
		patient.put("nurse_bed_confirmation", false);
		return patient;
	}

	public static HashMap<String, Object> sendEmergencie(int msg_id, int msg_priority, String cerinta, String name,
			int floor, int room, int bed, int heart_rate, int blood_presure, int body_temperature,
			int oxygen_saturation) {
		HashMap<String, Object> map = new HashMap<>();
		HashMap<String, Object> content = new HashMap<>();
		HashMap<String, Object> patient = patientMap(cerinta, name, floor, room, bed, heart_rate, blood_presure,
				body_temperature, oxygen_saturation);
		content.put("msg_id", msg_id);
		content.put("msg_priority", msg_priority);
		content.put("patient", patient);
		map.put("packet_type", "nurse_notification");
		map.put("content", content);
		return map;
	}

	public static JSONObject sendEmerginciesList() {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		JSONObject pacient = new JSONObject(
				sendEmergencie(1, 2, "Vreau mancare", "Florescu Mihai", 12, 3, 4, 100, 90, 36, 80));
		JSONObject pacient1 = new JSONObject(
				sendEmergencie(2, 2, "Vreau apa", "Popescu Marian", 2, 33, 7, 120, 90, 36, 90));
		JSONObject pacient2 = new JSONObject(
				sendEmergencie(3, 2, "Vreau perna ridicata", "Frentescu Andrei", 4, 3, 8, 107, 90, 38, 100));
		JSONObject pacient3 = new JSONObject(
				sendEmergencie(4, 2, "Vreau sa ma uit la televizor", "Popovici Cristian", 1, 13, 34, 90, 90, 37, 98));
		array.add((Object) pacient);
		array.add((Object) pacient1);
		array.add((Object) pacient2);
		array.add((Object) pacient3);
		json.put("packet_type", "emergencies_list");
		json.put("Emergencies", array);
		return json;
	}

	public static void main(String[] args) {
		int portNumber = 11000;
		PrintWriter out;
		BufferedReader in;
		String inputLine;
		String outputLine;
		JSONParser parser = new JSONParser();
		try {
			//out=new PrintWriter("asd");
			//new MyThread(out).start();
			System.out.println("Astept client.");
			ServerSocket serverSocket = new ServerSocket(portNumber);
			System.out.println("asd");
			Socket clientSocket = serverSocket.accept();
			System.out.println("Client Acceptat.");
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			MyThread thread = new MyThread(out);
			thread.start();
			boolean exit = true;
			while (exit) {
				thread.setSended(false);
				// aici primesc cerinta de login
				String sir = in.readLine();
				Object obj;
				JSONArray array;
				JSONObject obj2;
				System.out.println("Am primit de la aplicatie urmatoarele date de login: " + sir);
				obj = parser.parse("[" + sir + "]");
				array = (JSONArray) obj;
				obj2 = (JSONObject) array.get(0);
				sir = obj2.get("content").toString();
				obj = parser.parse("[" + sir + "]");
				array = (JSONArray) obj;
				obj2 = (JSONObject) array.get(0);
				System.out.println(obj2.get("username"));
				System.out.println(obj2.get("password"));

				// o sa verific daca trebuie sa se conecteze sau nu, iar in caz
				// afirmativ face ce urmeaza
				if (obj2.get("username").equals("Maria") && obj2.get("password").equals("1234")) {
					JSONObject jsonLogin = new JSONObject(sendLoginAccepted(true));
					JSONObject jsonLogout = new JSONObject(sendLogoutConfirmed());
					JSONObject jsonEmergencies = new JSONObject(sendEmerginciesList());
					int k = 0;
					if (thread.isAuto())
						out.println(jsonLogin);
					else {
						if (!thread.getSended()) {
							try {
								Thread.sleep(10000);
							} catch (Exception e) {

							}
							k = 1;
						}
					}
					while (k == 0) {
						sir = in.readLine();
						thread.setReceiveText(sir);
						System.out.println(sir);
						obj = parser.parse("[" + sir + "]");
						array = (JSONArray) obj;
						obj2 = (JSONObject) array.get(0);

						try {
							if (obj2.get("packet_type").equals("logout")) {
								out.println(jsonLogout);
								out.close();
								serverSocket.close();
								k = 1;
								System.out.println("Asistenta a dat logout");
							}

							if (obj2.get("packet_type").equals("Emergencies Request")) {
								out.println(jsonEmergencies);
								// k=1;
							}

							if (obj2.get("packet_type").equals("msg_ack")) {
								System.out.println("Notificare primita");
							}

							if (obj2.get("packet_type").equals("ping_ack")) {
								System.out.println("Ping receptionat inapoi de la aplicatie.");
							}

							if (obj2.get("packet_type").equals("delete_ack")) {
								System.out.println("S-a sters pacientul din lista asistentei");
							}
						} catch (IOException e) {

						}
					}
				} else {
					JSONObject jsonLogin = new JSONObject(sendLoginAccepted(false));
					System.out.println(jsonLogin);
					out.println(jsonLogin);
					out.close();
					serverSocket.close();
				}
			}

		} catch (Exception e) {
			System.out.println("Nu s-a deschis socketu-ul");
			// TODO: handle exception
		}
	}

}
