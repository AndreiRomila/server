import java.awt.Color;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Popup;

import org.json.simple.JSONObject;

public class MyThread extends Thread {
	private static PrintWriter out;
	private JFrame frame;
	private JPanel myPanel;
	private JButton sendPing = new JButton("Send Ping");
	private JButton login_generateJSON = new JButton("Generate JSON");

	static boolean k;
	private JButton sendLogin = new JButton("Send Login");
	private JRadioButton automat_Login = new JRadioButton("Automat Conectare");
	private JRadioButton manual_Login = new JRadioButton("Manual Conectare");
	private JTextField login_user = new JTextField("User");
	private JTextField login_id_session = new JTextField("ID_Session");
	private JTextField login_FirstName = new JTextField("First Name");
	private JTextField login_LastName = new JTextField("Last Name");
	private JTextField login_id = new JTextField("ID");
	private JTextField login_departament_id = new JTextField("Departament id");
	private JTextArea login_JSON_text = new JTextArea("JSON");
	private JButton load_default_login = new JButton("Load Default");

	private JButton sendEmergencie = new JButton("Send Emergencie");
	private JButton generateEmergencie = new JButton("Generate Emergencie");
	private JTextField emergenciesmsg_id = new JTextField("12");
	private JTextField emergenciesmsg_priority = new JTextField("1");
	private JTextField emergenciescerinta = new JTextField("Vreau mancare");
	private JTextField emergenciesname = new JTextField("Maria");
	private JTextField emergenciesfloor = new JTextField("2");
	private JTextField emergenciesroom = new JTextField("23");
	private JTextField emergenciesbed = new JTextField("2");
	private JTextField emergenciesheart_rate = new JTextField("93");
	private JTextField emergenciesblood_presure = new JTextField("90");
	private JTextField emergenciesbody_temperature = new JTextField("37");
	private JTextField emergenciesoxygen_saturation = new JTextField("70");
	private JRadioButton emergenciesheart_rate_alarm = new JRadioButton("Alarm");
	private JRadioButton emergenciesblood_presure_alarm = new JRadioButton("Alarm");
	private JRadioButton emergenciesbody_temperature_alarm = new JRadioButton("Alarm");
	private JRadioButton emergenciesoxygen_saturation_alarm = new JRadioButton("Alarm");
	private JTextArea emergencie_JSON_text = new JTextArea("JSON");

	private JButton load_default_emergencies = new JButton("Load Default");

	private JTextArea ping_JSON_text = new JTextArea("JSON");

	private JButton delete_Emergencie = new JButton("Delete Emergencie");
	private JTextArea delete_JSON_text = new JTextArea("JSON");
	private JTextField delete_msg_id = new JTextField("1");

	private JTextArea Send = new JTextArea();
	private JTextArea Receive = new JTextArea();
	private boolean sended;

	public MyThread(PrintWriter out) {
		this.out = out;
		frame = new JFrame("Server");
		myPanel = new JPanel();
		frame.add(myPanel);
		frame.setSize(1800, 1300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		myPanel.setLayout(null);
		setSendText();
		setReceiveText();
		setConectare();
		setEmergencie();
		setDelete();
		setPing();
		sended = false;
		k = true;

	}

	public void setSended(boolean x) {
		sended = x;
	}

	public boolean getSended() {
		return sended;
	}

	public void setReceiveText(String s) {
		Receive.setText(Receive.getText() + "\n" + s);
	}

	private void setSendText() {
		JTextField titlu = new JTextField("Send");
		titlu.setEditable(false);
		titlu.setSize(200, 30);
		titlu.setLocation(100, 460);
		titlu.setBackground(Color.GRAY);
		myPanel.add(titlu);

		Send.setSize(500, 300);
		Send.setLocation(30, 500);
		Send.setEditable(false);
		JScrollPane scroll = new JScrollPane(Send, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setSize(500, 300);
		scroll.setLocation(30, 500);
		myPanel.add(scroll);
	}

	private void setReceiveText() {
		JTextField titlu = new JTextField("Receive");
		titlu.setEditable(false);
		titlu.setSize(200, 30);
		titlu.setLocation(700, 460);
		titlu.setBackground(Color.GRAY);
		myPanel.add(titlu);

		Receive.setSize(500, 300);
		Receive.setLocation(600, 500);
		Receive.setEditable(false);
		JScrollPane scroll = new JScrollPane(Receive, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setSize(500, 300);
		scroll.setLocation(600, 500);
		myPanel.add(scroll);
	}

	public boolean isAuto() {
		return automat_Login.isSelected();
	}

	private void setConectare() {

		sendLogin.setSize(150, 30);
		sendLogin.setLocation(200, 50);
		myPanel.add(sendLogin);

		login_generateJSON.setSize(150, 30);
		login_generateJSON.setLocation(200, 100);
		myPanel.add(login_generateJSON);

		automat_Login.setSize(150, 30);
		automat_Login.setLocation(30, 50);
		automat_Login.setSelected(true);
		sendLogin.setEnabled(false);
		login_generateJSON.setEnabled(false);
		myPanel.add(automat_Login);

		manual_Login.setSize(150, 30);
		manual_Login.setLocation(30, 100);
		myPanel.add(manual_Login);

		login_departament_id.setSize(150, 30);
		login_departament_id.setLocation(30, 100);
		myPanel.add(login_departament_id);

		/*
		 * .setSize(150, 30); .setLocation(30, 100); myPanel.add();
		 */
		login_id.setSize(100, 30);
		login_id.setLocation(30, 180);
		myPanel.add(login_id);

		login_FirstName.setSize(100, 30);
		login_FirstName.setLocation(30, 140);
		myPanel.add(login_FirstName);

		login_id_session.setSize(100, 30);
		login_id_session.setLocation(30, 220);
		myPanel.add(login_id_session);

		login_LastName.setSize(100, 30);
		login_LastName.setLocation(150, 140);
		myPanel.add(login_LastName);

		login_user.setSize(100, 30);
		login_user.setLocation(150, 180);
		myPanel.add(login_user);

		login_departament_id.setSize(150, 30);
		login_departament_id.setLocation(150, 220);
		myPanel.add(login_departament_id);

		login_JSON_text.setSize(300, 180);
		login_JSON_text.setLocation(30, 260);
		login_JSON_text.setAutoscrolls(true);
		JScrollPane scroll = new JScrollPane(login_JSON_text, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setSize(300, 180);
		scroll.setLocation(30, 260);
		myPanel.add(scroll);

		JTextField titlu = new JTextField("Conectare");
		titlu.setEditable(false);
		titlu.setSize(150, 30);
		titlu.setLocation(100, 10);
		myPanel.add(titlu);
		load_default_login.setSize(150, 30);
		load_default_login.setLocation(300, 10);
		load_default_login.setVisible(true);
		load_default_login.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (k) {
					login_user.setText("User1");
					login_id_session.setText("12as4");
					login_FirstName.setText("Maria");
					login_LastName.setText("Popescu");
					login_id.setText("124");
					login_departament_id.setText("21");

					emergenciesmsg_id.setText("12");
					emergenciesmsg_priority.setText("1");
					emergenciescerinta.setText("Vreau mancare");
					emergenciesname.setText("Maria");
					emergenciesfloor.setText("2");
					emergenciesroom.setText("23");
					emergenciesbed.setText("2");
					emergenciesheart_rate.setText("93");
					emergenciesblood_presure.setText("90");
					emergenciesbody_temperature.setText("37");
					emergenciesoxygen_saturation.setText("70");
					k = false;
					delete_msg_id.setText("1");
				} else {
					k = true;
					login_user.setText("User");
					login_id_session.setText("Session ID");
					login_FirstName.setText("First Name");
					login_LastName.setText("Last Name");
					login_id.setText("ID");
					login_departament_id.setText("Departament ID");

					emergenciesmsg_id.setText("Message ID");
					emergenciesmsg_priority.setText("Message Priority");
					emergenciescerinta.setText("Request");
					emergenciesname.setText("Name");
					emergenciesfloor.setText("Floor");
					emergenciesroom.setText("Room");
					emergenciesbed.setText("Bed");
					emergenciesheart_rate.setText("Heart Rate");
					emergenciesblood_presure.setText("Blood Presure");
					emergenciesbody_temperature.setText("Temperature");
					emergenciesoxygen_saturation.setText("Oxygen Saturation");

					delete_msg_id.setText("Message ID");
				}

			}
		});
		myPanel.add(load_default_login);

	}

	private void setEmergencie() {
		sendEmergencie.setSize(150, 30);
		sendEmergencie.setLocation(430, 50);
		myPanel.add(sendEmergencie);

		generateEmergencie.setSize(150, 30);
		generateEmergencie.setLocation(630, 50);
		myPanel.add(generateEmergencie);

		emergenciesmsg_id.setSize(150, 30);
		emergenciesmsg_id.setLocation(430, 100);
		myPanel.add(emergenciesmsg_id);

		emergenciesmsg_priority.setSize(150, 30);
		emergenciesmsg_priority.setLocation(430, 140);
		myPanel.add(emergenciesmsg_priority);

		emergenciescerinta.setSize(150, 30);
		emergenciescerinta.setLocation(430, 180);
		myPanel.add(emergenciescerinta);

		emergenciesname.setSize(150, 30);
		emergenciesname.setLocation(430, 220);
		myPanel.add(emergenciesname);

		emergenciesfloor.setSize(150, 30);
		emergenciesfloor.setLocation(430, 260);
		myPanel.add(emergenciesfloor);

		emergenciesroom.setSize(150, 30);
		emergenciesroom.setLocation(630, 220);
		myPanel.add(emergenciesroom);

		emergenciesbed.setSize(150, 30);
		emergenciesbed.setLocation(630, 260);
		myPanel.add(emergenciesbed);

		emergenciesheart_rate.setSize(150, 30);
		emergenciesheart_rate.setLocation(630, 100);
		myPanel.add(emergenciesheart_rate);
		
		emergenciesheart_rate_alarm.setSize(100, 30);
		emergenciesheart_rate_alarm.setLocation(780, 100);
		myPanel.add(emergenciesheart_rate_alarm);

		emergenciesblood_presure.setSize(150, 30);
		emergenciesblood_presure.setLocation(630, 140);
		myPanel.add(emergenciesblood_presure);
		
		emergenciesblood_presure_alarm.setSize(100, 30);
		emergenciesblood_presure_alarm.setLocation(780, 140);
		myPanel.add(emergenciesblood_presure_alarm);

		emergenciesbody_temperature.setSize(150, 30);
		emergenciesbody_temperature.setLocation(630, 180);
		myPanel.add(emergenciesbody_temperature);
		
		emergenciesbody_temperature_alarm.setSize(100, 30);
		emergenciesbody_temperature_alarm.setLocation(780, 180);
		myPanel.add(emergenciesbody_temperature_alarm);

		emergenciesoxygen_saturation.setSize(150, 30);
		emergenciesoxygen_saturation.setLocation(430, 300);
		myPanel.add(emergenciesoxygen_saturation);
		
		emergenciesoxygen_saturation_alarm.setSize(100, 30);
		emergenciesoxygen_saturation_alarm.setLocation(580, 300);
		myPanel.add(emergenciesoxygen_saturation_alarm);

		emergencie_JSON_text.setSize(350, 100);
		emergencie_JSON_text.setLocation(430, 340);
		JScrollPane scroll = new JScrollPane(emergencie_JSON_text, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setSize(350, 100);
		scroll.setLocation(430, 340);
		myPanel.add(scroll);

		JTextField titlu = new JTextField("Notificare");
		titlu.setEditable(false);
		titlu.setSize(150, 30);
		titlu.setLocation(500, 10);
		myPanel.add(titlu);
	}

	private void setPing() {
		sendPing.setSize(150, 30);
		sendPing.setLocation(900, 50);
		myPanel.add(sendPing);

		ping_JSON_text.setSize(350, 200);
		ping_JSON_text.setLocation(830, 240);
		JScrollPane scroll = new JScrollPane(ping_JSON_text, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setSize(350, 200);
		scroll.setLocation(830, 240);
		myPanel.add(scroll);

		JTextField titlu = new JTextField("Ping");
		titlu.setEditable(false);
		titlu.setSize(150, 30);
		titlu.setLocation(900, 10);
		myPanel.add(titlu);

	}

	private void setDelete() {
		delete_Emergencie.setSize(150, 30);
		delete_Emergencie.setLocation(1300, 50);
		myPanel.add(delete_Emergencie);

		delete_msg_id.setSize(150, 30);
		delete_msg_id.setLocation(1300, 100);
		myPanel.add(delete_msg_id);

		delete_JSON_text.setSize(350, 200);
		delete_JSON_text.setLocation(1250, 240);
		JScrollPane scroll = new JScrollPane(delete_JSON_text, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setSize(350, 200);
		scroll.setLocation(1250, 240);
		myPanel.add(scroll);

		JTextField titlu = new JTextField("Delete");
		titlu.setEditable(false);
		titlu.setSize(150, 30);
		titlu.setLocation(1300, 10);
		myPanel.add(titlu);
	}

	public static HashMap<String, Object> verifyConnexion() {
		HashMap<String, Object> map = new HashMap<>();
		HashMap<String, Object> content = new HashMap<>();
		// add content fields
		content.put("connection_status", 1);
		// add map fields
		map.put("packet_type", "ping");
		map.put("content", content);
		return map;
	}

	public static HashMap<String, Object> deleteEmergencie(int msg_id) {
		HashMap<String, Object> map = new HashMap<>();
		HashMap<String, Object> content = new HashMap<>();
		// add content fields
		content.put("msg_id", msg_id);
		// add map fields
		map.put("packet_type", "delete");
		map.put("content", content);
		return map;
	}

	public static HashMap<String, Object> sendLoginAccepted(String user, String id_session, String FirstName,
			String LastName, int id, int departament_id) {
		HashMap<String, Object> map = new HashMap<>();
		HashMap<String, Object> content = new HashMap<>();
		HashMap<String, Object> settings = new HashMap<>();
		// add content fields
		content.put("status", true);
		content.put("error", 0);
		// add settings fields
		settings.put("Username", user);
		settings.put("SessionId", id_session);
		settings.put("FirstName", FirstName);
		settings.put("LastName", LastName);
		settings.put("Id", id);
		settings.put("DepartmentId", departament_id);
		// add map fields
		map.put("packet_type", "login_ack");
		map.put("content", content);
		map.put("settings", settings);
		return map;
	}

	public static HashMap<String, Object> biometricsMap(int heart, boolean heart_alarm, int blood, boolean blood_alarm,
			int body, boolean body_alarm, int oxygen, boolean oxygen_alarm) {
		HashMap<String, Object> biometrics = new HashMap<>();
		HashMap<String, Object> blood_presure = new HashMap<>();
		HashMap<String, Object> heart_rate = new HashMap<>();
		HashMap<String, Object> O2 = new HashMap<>();
		HashMap<String, Object> body_temperature = new HashMap<>();
		blood_presure.put("value", blood);
		blood_presure.put("alarm", blood_alarm);
		heart_rate.put("value", heart);
		heart_rate.put("alarm", heart_alarm);
		O2.put("value", oxygen);
		O2.put("alarm", oxygen_alarm);
		body_temperature.put("value", body);
		body_temperature.put("alarm", body_alarm);
		biometrics.put("blood_presure", blood_presure);
		biometrics.put("heart_rate", heart_rate);
		biometrics.put("o2", O2);
		biometrics.put("body_temperature", body_temperature);
		return biometrics;
	}

	public static HashMap<String, Object> patientMap(String cerinta, String name, int floor, int room, int bed,
			int heart_rate, boolean heart_rate_alarm, int blood_presure, boolean blood_presure_alarm,
			int body_temperature, boolean body_temperature_alarm, int oxygen_saturation,
			boolean oxygen_saturation_alarm) {
		HashMap<String, Object> patient = new HashMap<>();
		HashMap<String, Object> biometrics = biometricsMap(heart_rate, heart_rate_alarm, blood_presure,
				blood_presure_alarm, body_temperature, body_temperature_alarm, oxygen_saturation,
				oxygen_saturation_alarm);
		HashMap<String, Object> request = new HashMap<>();
		request.put("value", cerinta);
		request.put("alarm", true);
		patient.put("id", "df58");
		patient.put("name", name);
		patient.put("floor", floor);
		patient.put("room", room);
		patient.put("bed", bed);
		patient.put("request", request);
		patient.put("biometrics", biometrics);
		patient.put("nurse_bed_confirmation", false);
		return patient;
	}

	public static HashMap<String, Object> sendEmergencie(int msg_id, int msg_priority, String cerinta, String name,
			int floor, int room, int bed, int heart_rate, boolean heart_rate_alarm, int blood_presure,
			boolean blood_presure_alarm, int body_temperature, boolean body_temperature_alarm, int oxygen_saturation,
			boolean oxygen_saturation_alarm) {
		HashMap<String, Object> map = new HashMap<>();
		HashMap<String, Object> content = new HashMap<>();
		HashMap<String, Object> patient = patientMap(cerinta, name, floor, room, bed, heart_rate, heart_rate_alarm,
				blood_presure, blood_presure_alarm, body_temperature, body_temperature_alarm, oxygen_saturation,
				oxygen_saturation_alarm);
		content.put("msg_id", msg_id);
		content.put("msg_priority", msg_priority);
		content.put("patient", patient);
		map.put("packet_type", "nurse_notification");
		map.put("content", content);
		return map;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		login_id.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(login_id.getText().toString());
					login_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					login_id.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(login_id.getText().toString());
					login_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					login_id.setBackground(Color.RED);
				}
			}
		});
		login_id.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(login_id.getText().toString());
					login_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					login_id.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					login_id.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
		login_departament_id.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(login_departament_id.getText().toString());
					login_departament_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					login_departament_id.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(login_departament_id.getText().toString());
					login_departament_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					login_departament_id.setBackground(Color.RED);
				}
			}
		});
		login_departament_id.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(login_departament_id.getText().toString());
					login_departament_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					login_departament_id.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					login_departament_id.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesmsg_id.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesmsg_id.getText().toString());
					emergenciesmsg_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesmsg_id.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesmsg_id.getText().toString());
					emergenciesmsg_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesmsg_id.setBackground(Color.RED);
				}
			}
		});
		emergenciesmsg_id.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesmsg_id.getText().toString());
					emergenciesmsg_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesmsg_id.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesmsg_id.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesmsg_priority.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesmsg_priority.getText().toString());
					emergenciesmsg_priority.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesmsg_priority.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesmsg_priority.getText().toString());
					emergenciesmsg_priority.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesmsg_priority.setBackground(Color.RED);
				}
			}
		});
		emergenciesmsg_priority.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesmsg_priority.getText().toString());
					emergenciesmsg_priority.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesmsg_priority.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesmsg_priority.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesfloor.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesfloor.getText().toString());
					emergenciesfloor.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesfloor.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesfloor.getText().toString());
					emergenciesfloor.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesfloor.setBackground(Color.RED);
				}
			}
		});
		emergenciesfloor.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesfloor.getText().toString());
					emergenciesfloor.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesfloor.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesfloor.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesroom.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesroom.getText().toString());
					emergenciesroom.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesroom.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesroom.getText().toString());
					emergenciesroom.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesroom.setBackground(Color.RED);
				}
			}
		});
		emergenciesroom.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesroom.getText().toString());
					emergenciesroom.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesroom.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesroom.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesbed.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesbed.getText().toString());
					emergenciesbed.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesbed.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesbed.getText().toString());
					emergenciesbed.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesbed.setBackground(Color.RED);
				}
			}
		});
		emergenciesbed.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesbed.getText().toString());
					emergenciesbed.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesbed.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesbed.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesheart_rate.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesheart_rate.getText().toString());
					emergenciesheart_rate.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesheart_rate.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesheart_rate.getText().toString());
					emergenciesheart_rate.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesheart_rate.setBackground(Color.RED);
				}
			}
		});
		emergenciesheart_rate.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesheart_rate.getText().toString());
					emergenciesheart_rate.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesheart_rate.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesheart_rate.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesblood_presure.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesblood_presure.getText().toString());
					emergenciesblood_presure.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesblood_presure.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesblood_presure.getText().toString());
					emergenciesblood_presure.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesblood_presure.setBackground(Color.RED);
				}
			}
		});
		emergenciesblood_presure.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesblood_presure.getText().toString());
					emergenciesblood_presure.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesblood_presure.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesblood_presure.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesbody_temperature.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesbody_temperature.getText().toString());
					emergenciesbody_temperature.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesbody_temperature.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesbody_temperature.getText().toString());
					emergenciesbody_temperature.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesbody_temperature.setBackground(Color.RED);
				}
			}
		});
		emergenciesbody_temperature.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesbody_temperature.getText().toString());
					emergenciesbody_temperature.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesbody_temperature.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesbody_temperature.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		emergenciesoxygen_saturation.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesoxygen_saturation.getText().toString());
					emergenciesoxygen_saturation.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesoxygen_saturation.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesoxygen_saturation.getText().toString());
					emergenciesoxygen_saturation.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesoxygen_saturation.setBackground(Color.RED);
				}
			}
		});
		emergenciesoxygen_saturation.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesoxygen_saturation.getText().toString());
					emergenciesoxygen_saturation.setBackground(Color.WHITE);
				} catch (Exception f) {
					emergenciesoxygen_saturation.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					emergenciesoxygen_saturation.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		delete_msg_id.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(delete_msg_id.getText().toString());
					delete_msg_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					delete_msg_id.setBackground(Color.RED);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(delete_msg_id.getText().toString());
					delete_msg_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					delete_msg_id.setBackground(Color.RED);
				}
			}
		});
		delete_msg_id.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(delete_msg_id.getText().toString());
					delete_msg_id.setBackground(Color.WHITE);
				} catch (Exception f) {
					delete_msg_id.setBackground(Color.RED);
				}
				int key = e.getKeyChar();
				if (!Character.isDigit(key)) {
					delete_msg_id.setBackground(Color.RED);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		generateEmergencie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(emergenciesmsg_id.getText());
					Integer.parseInt(emergenciesmsg_priority.getText());
					Integer.parseInt(emergenciesfloor.getText());
					Integer.parseInt(emergenciesfloor.getText());
					Integer.parseInt(emergenciesbed.getText());
					Integer.parseInt(emergenciesheart_rate.getText());
					Integer.parseInt(emergenciesblood_presure.getText());
					Integer.parseInt(emergenciesbody_temperature.getText());
					Integer.parseInt(emergenciesoxygen_saturation.getText());
					emergencie_JSON_text
							.setText((new JSONObject(sendEmergencie(Integer.parseInt(emergenciesmsg_id.getText()),
									Integer.parseInt(emergenciesmsg_priority.getText()), emergenciescerinta.getText(),
									emergenciesname.getText(), Integer.parseInt(emergenciesfloor.getText()),
									Integer.parseInt(emergenciesroom.getText()),
									Integer.parseInt(emergenciesbed.getText()),
									Integer.parseInt(emergenciesheart_rate.getText()),
									emergenciesheart_rate_alarm.isSelected(),
									Integer.parseInt(emergenciesblood_presure.getText()),
									emergenciesblood_presure_alarm.isSelected(),
									Integer.parseInt(emergenciesbody_temperature.getText()),
									emergenciesbody_temperature_alarm.isSelected(),
									Integer.parseInt(emergenciesoxygen_saturation.getText()),
									emergenciesoxygen_saturation_alarm.isSelected()))).toString());

				} catch (Exception f) {
					JOptionPane.showMessageDialog(frame, "Boxes wwich are red have integer values");
				}

			}
		});
		sendEmergencie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (emergencie_JSON_text.getText().toString().equals("JSON")) {
					try {
						Integer.parseInt(emergenciesmsg_id.getText());
						Integer.parseInt(emergenciesmsg_priority.getText());
						Integer.parseInt(emergenciesfloor.getText());
						Integer.parseInt(emergenciesfloor.getText());
						Integer.parseInt(emergenciesbed.getText());
						Integer.parseInt(emergenciesheart_rate.getText());
						Integer.parseInt(emergenciesblood_presure.getText());
						Integer.parseInt(emergenciesbody_temperature.getText());
						Integer.parseInt(emergenciesoxygen_saturation.getText());
						out.println(new JSONObject(sendEmergencie(Integer.parseInt(emergenciesmsg_id.getText()),
								Integer.parseInt(emergenciesmsg_priority.getText()), emergenciescerinta.getText(),
								emergenciesname.getText(), Integer.parseInt(emergenciesfloor.getText()),
								Integer.parseInt(emergenciesroom.getText()), Integer.parseInt(emergenciesbed.getText()),
								Integer.parseInt(emergenciesheart_rate.getText()),
								emergenciesheart_rate_alarm.isSelected(),
								Integer.parseInt(emergenciesblood_presure.getText()),
								emergenciesblood_presure_alarm.isSelected(),
								Integer.parseInt(emergenciesbody_temperature.getText()),
								emergenciesbody_temperature_alarm.isSelected(),
								Integer.parseInt(emergenciesoxygen_saturation.getText()),
								emergenciesoxygen_saturation_alarm.isSelected())));
						Send.setText(Send.getText() + "\n"
								+ new JSONObject(sendEmergencie(Integer.parseInt(emergenciesmsg_id.getText()),
										Integer.parseInt(emergenciesmsg_priority.getText()),
										emergenciescerinta.getText(), emergenciesname.getText(),
										Integer.parseInt(emergenciesfloor.getText()),
										Integer.parseInt(emergenciesroom.getText()),
										Integer.parseInt(emergenciesbed.getText()),
										Integer.parseInt(emergenciesheart_rate.getText()),
										emergenciesheart_rate_alarm.isSelected(),
										Integer.parseInt(emergenciesblood_presure.getText()),
										emergenciesblood_presure_alarm.isSelected(),
										Integer.parseInt(emergenciesbody_temperature.getText()),
										emergenciesbody_temperature_alarm.isSelected(),
										Integer.parseInt(emergenciesoxygen_saturation.getText()),
										emergenciesoxygen_saturation_alarm.isSelected())));
						System.out.println("Am trimis Emergencie");
					} catch (Exception f) {
						JOptionPane.showMessageDialog(frame, "Boxes which are red must have integer values");
					}
				} else {
					out.println(emergencie_JSON_text.getText().toString());
					Send.setText(Send.getText() + "\n" + emergencie_JSON_text.getText().toString());
				}
			}
		});
		manual_Login.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				automat_Login.setSelected(false);
				manual_Login.setSelected(true);
				sendLogin.setEnabled(true);
				login_generateJSON.setEnabled(true);
			}
		});
		automat_Login.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				manual_Login.setSelected(false);
				automat_Login.setSelected(true);
				sendLogin.setEnabled(false);
				login_generateJSON.setEnabled(false);
			}
		});
		login_generateJSON.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int k = 0;
				try {
					Integer.parseInt(login_id.getText().toString());
					Integer.parseInt(login_id.getText().toString());
					login_JSON_text.setText((new JSONObject(sendLoginAccepted(login_user.getText(),
							login_id_session.getText(), login_FirstName.getText(), login_LastName.getText(),
							Integer.parseInt(login_id.getText().toString()),
							Integer.parseInt(login_departament_id.getText().toString())))).toString());
				} catch (Exception f) {
					JOptionPane.showMessageDialog(frame, "ID must be integer\n Departament ID must be integer");
				}
			}
		});
		sendLogin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Am trimis datele de logare.");
				if (automat_Login.isSelected()) {
					try {
						Integer.parseInt(login_id.getText().toString());
						Integer.parseInt(login_id.getText().toString());
						out.println(new JSONObject(sendLoginAccepted(login_user.getText(), login_id_session.getText(),
								login_FirstName.getText(), login_LastName.getText(),
								Integer.parseInt(login_id.getText().toString()),
								Integer.parseInt(login_departament_id.getText().toString()))));
						Send.setText(Send.getText() + "\n"
								+ new JSONObject(sendLoginAccepted(login_user.getText(), login_id_session.getText(),
										login_FirstName.getText(), login_LastName.getText(),
										Integer.parseInt(login_id.getText().toString()),
										Integer.parseInt(login_departament_id.getText().toString()))));
					} catch (Exception f) {
						out.println(new JSONObject(sendLoginAccepted(login_user.getText(), login_id_session.getText(),
								login_FirstName.getText(), login_LastName.getText(), 1, 1)));
						Send.setText(Send.getText() + "\n"
								+ new JSONObject(sendLoginAccepted(login_user.getText(), login_id_session.getText(),
										login_FirstName.getText(), login_LastName.getText(), 1, 1)));
					}
				} else {
					try {
						Integer.parseInt(login_id.getText().toString());
						Integer.parseInt(login_id.getText().toString());
						Send.setText(Send.getText() + "\n" + login_JSON_text.getText());
						out.println(login_JSON_text.getText());
					} catch (Exception f) {
						JOptionPane.showMessageDialog(frame, "ID must be integer\n Departament ID must be integer");
					}
				}
			}
		});
		sendPing.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Am trimis ping");
				out.println(new JSONObject(verifyConnexion()));
				ping_JSON_text.setText((new JSONObject(verifyConnexion())).toString());
				Send.setText(Send.getText() + "\n" + (new JSONObject(verifyConnexion())).toString());
			}
		});
		delete_Emergencie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					Integer.parseInt(delete_msg_id.getText().toString());
					out.println(new JSONObject(deleteEmergencie(Integer.parseInt(delete_msg_id.getText().toString()))));
					Send.setText(
							Send.getText() + "\n"
									+ (new JSONObject(
											deleteEmergencie(Integer.parseInt(delete_msg_id.getText().toString()))))
													.toString());
					delete_JSON_text.setText(
							(new JSONObject(deleteEmergencie(Integer.parseInt(delete_msg_id.getText().toString()))))
									.toString());
					System.out.println("Am trimis cerere de stergere pacient din lista");
				} catch (Exception f) {
					JOptionPane.showMessageDialog(frame, "Mesage ID must be integer");
				}
			}
		});
	}

}
